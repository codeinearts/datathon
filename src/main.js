// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import Router from 'vue-router'
import VueSession from 'vue-session'
import VueCookie from 'vue-cookie'

// Visual imports
import icons from 'glyphicons'
import Vuetify from 'vuetify'
import VTooltip from 'v-tooltip'

// Vue importing
// Vue.use(VueSession, { persist: true })
// Vue.use(VueCookie)
// Vue.use(Vuex)
Vue.use(Router)
Vue.use(Vuetify, {
  theme: {
    primary: '#7b87a8',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})
Vue.use(VTooltip)

import 'vuetify/dist/vuetify.min.css'
import './assets/style.scss'

// Router components
import App from '@/App'
import router from '@/router'
// import store from '@/store'

/**
 * For Logging on inline elements
 */
Vue.prototype.$log = console.log.bind(console)

Vue.mixin({
  methods: {
    // Gets rid of reactive stuff when printing data objects
    purify: reactiveObject => {
      return JSON.parse(JSON.stringify(reactiveObject))
    }
  }
})

/**
 * to: Route: the target Route Object being navigated to.
 * from: Route: the current route being navigated away from
 * next: function: this function must be called to resolve the hook.
 * action depends on the arguments provided to next.
 */
// router.beforeEach((to, from, next) => {
//   if (to.matched.some(record => record.meta.requiresAuth)) {
//     // navigation requires auth, dispatch
//     store.dispatch('AUTH_TOKEN_CHECK').then(tokenValid => {
//       if (!tokenValid) {
//         alert('Acceso denegado')
//         next('/login')
//       } else {
//         // continue navigation
//         next()
//       }
//     })
//   } else {
//     // route without authorization required
//     next()
//   }
// })

Vue.config.productionTip = false

/* eslint-disable no-new */
const app = new Vue({
  el: '#app',
  // store,
  router,
  template: '<App/>',
  components: { App }
})
