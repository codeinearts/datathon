/**********************************************************
 *                     MAIN ROUTER                        *
 **********************************************************/

import Vue from 'vue'
import Router from 'vue-router'
let d3Test = () =>
  import ('src/views/vueD3')
let pantalla_uno = () =>
  import ('src/views/pantalla_uno')
let pantalla_dos = () =>
  import ('src/views/pantalla_dos')
let pantalla_tres = () =>
  import ('src/views/pantalla_tres')
let pantalla_cuatro = () =>
  import ('src/views/pantalla_cuatro')
let pantalla_cinco = () =>
  import ('src/views/pantalla_cinco')
let pantalla_seis = () =>
  import ('src/views/pantalla_seis')
let pantalla_siete = () =>
  import ('src/views/pantalla_siete')
let pantalla_ocho = () =>
  import ('src/views/pantalla_ocho')
let pantalla_nueve = () =>
  import ('src/views/pantalla_nueve')

export default new Router({
  routes: [{
      path: '/',
      alias: ['/inicio', '/screen1'],
      component: pantalla_uno
    },
    {
      path: '/impuestos_en_chile',
      alias: ['/screen2'],
      component: pantalla_dos
    },
    {
      path: '/pago_impuestos',
      alias: ['/screen3'],
      component: pantalla_tres
    },
    {
      path: '/horas_trabajo_impuestos',
      alias: ['/screen4'],
      component: pantalla_cuatro
    },
    {
      path: '/recaudacion_impuestos',
      alias: ['/screen5'],
      component: pantalla_cinco
    },
    {
      path: '/total_impuestos',
      alias: ['/screen6'],
      component: pantalla_seis
    },
    {
      path: '/comparaciones',
      alias: ['/screen7'],
      component: pantalla_siete
    },
    /**
     * RUTA FALTANTE 7.5
     */
    // {
    //   path: '/creación_presupuesto',
    //   alias: ['/missing_screen'],
    //   component: pantalla_siete
    // },
    {
      path: '/pre_visualizacion',
      alias: ['/screen8'],
      component: pantalla_ocho
    },
    /**
     * Yoyo's elements
     */
    {
      path: '/visualizaciones',
      alias: ['/screen9'],
      component: null
    },
    {
      path: '/conocenos',
      alias: ['/screen10'],
      component: pantalla_nueve
    },
    {
      path: '/vdata',
      beforeEnter() {
        location.href = '/static/graficos/partida.html'
      }
    }
  ]
})
