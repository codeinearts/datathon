'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const EventSchema = Schema({
  subject: String,
  description: String,
  created: Date,
  guest_users_id: { type: [Schema.Types.ObjectId] },
  type: { type: String, enum: ['born', 'global', 'meeting', 'agenda'] },
  date: Date
})

module.exports = mongoose.model('Events', EventSchema, 'Events')
