'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const InfoSchema = Schema({
  doc_type: { type: String, enum: ['mission_vision', 'organization_chart', 'welcome_message', 'birth_message', 'org_chart', 'policies', 'certs', 'peer_commitee', 'social', 'links'] },
  meta: {
    title: String,
    content: String,
    list: [],
    images_url: []
  }
})

module.exports = mongoose.model('Info', InfoSchema, 'Info')
