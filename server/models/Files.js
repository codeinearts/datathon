'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const FileSchema = Schema({
  name: String,
  type: String,
  creator: {
    _id: { type: Schema.Types.ObjectId },
    section: String,
    name: String,
    date: Date
  },
  available: String,
  url: String,
  created: Date
})

module.exports = mongoose.model('Files', FileSchema, 'Files')
