'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const VideoSchema = Schema({
  name: String,
  description: String,
  creator: {
    _id: { type: Schema.Types.ObjectId },
    name: String,
    date: Date
  },
  tags: []
})

module.exports = mongoose.model('Videos', VideoSchema, 'Videos')
