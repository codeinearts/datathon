// Archivo para definir las rutas de la api RESTful.

'use strict'

const express = require('express')
const api = express.Router()
const PositionController = require('../controllers/Positions.js')
const DepartmentController = require('../controllers/Departments.js')
const InfoController = require('../controllers/Info.js')
const ProjectController = require('../controllers/Projects.js')
const VideoController = require('../controllers/Videos.js')
const EventController = require('../controllers/Events.js')
const NewsController = require('../controllers/News.js')
const FileController = require('../controllers/Files.js')
const RequestController = require('../controllers/Requests.js')

// Operaciones sobre colección Positions
api.get('/position/:id', PositionController.GetPosition)
api.get('/position', PositionController.GetPositions)
api.post('/position', PositionController.NewPosition)
api.put('/position/:id', PositionController.UpdatePosition)
api.delete('/position/:id', PositionController.DeletePosition)

// Operaciones sobre colección Departments
api.get('/department/:id', DepartmentController.GetDepartment)
api.get('/department', DepartmentController.GetDepartments)
api.post('/department', DepartmentController.NewDepartment)
api.put('/department/:id', DepartmentController.UpdateDepartment)
api.delete('/department/:id', DepartmentController.DeleteDepartment)

// Operaciones sobre colección Info
api.get('/info/:doc_type', InfoController.GetInfo)
api.post('/info', InfoController.NewInfo)
api.put('/info/:doc_type', InfoController.UpdateInfo)
api.delete('/info/:doc_type', InfoController.DeleteInfo)

// Operaciones sobre colección Projects
api.get('/project/:id', ProjectController.GetProject)
api.get('/project', ProjectController.GetProjects)
api.post('/project', ProjectController.NewProject)
api.put('/project/:id', ProjectController.UpdateProject)
api.delete('/project/:id', ProjectController.DeleteProject)

// Operaciones sobre colección Videos
api.get('/video/:id', VideoController.GetVideo)
api.get('/video', VideoController.GetVideos)
api.post('/video', VideoController.NewVideo)
api.put('/video/:id', VideoController.UpdateVideo)
api.delete('/video/:id', VideoController.DeleteVideo)

// Operaciones sobre colección Events
api.get('/event/:id', EventController.GetEvent)
api.get('/event', EventController.GetEvents)
api.post('/event', EventController.NewEvent)
api.put('/event/:id', EventController.UpdateEvent)
api.delete('/event/:id', EventController.DeleteEvent)

// Operaciones sobre colección News
api.get('/news/:modificador', NewsController.GetNews)
api.post('/news', NewsController.NewNews)
api.put('/news/:id', NewsController.UpdateNews)
api.delete('/news/:id', NewsController.DeleteNews)

// Operaciones sobre colección Files
api.get('/file/:id', FileController.GetFile)
api.get('/file', FileController.GetFiles)
api.post('/file', FileController.NewFile)
api.put('/file/:id', FileController.UpdateFile)
api.delete('/file/:id', FileController.DeleteFile)

// Operaciones sobre colección Requests
api.get('/request/:id', RequestController.GetRequest)
api.get('/request', RequestController.GetRequests)
api.post('/request', RequestController.NewRequest)
api.put('/request/:id', RequestController.UpdateRequest)
api.delete('/request/:id', RequestController.DeleteRequest)

/**
 * TESTS AND AUTH SERVICE ENDPOINTS
 */
const _authService = require('./auth.js')
const _testService = require('./test.js')
api.use('/auth', _authService)
api.use('/test', _testService)

module.exports = api
