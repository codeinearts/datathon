'use strict'

const News = require('../models/News.js')

function GetNews (req, res) {
  let modificador = req.params.modificador

  switch (modificador) {
    case 'obtenerTodo':
      News.find({}, (err, news) => {
        if (err) {
          return res.status(500).send({ message: 'Ha ocurrido un error al consultar la base de datos. ' + err })
        }

        res.status(200).send({ news: news })
      })
      break

    case 'obtenerUnoPorId':
      let id = req.query.id

      News.findById(id, (err, news) => {
        if (err) {
          return res.status(500).send({ message: 'Ha ocurrido un error al consultar la base de datos. ' + err })
        }
        if (!news) {
          return res.status(404).send({ message: 'El elemento no existe en la base de datos.' })
        }

        res.status(200).send({ news: news })
      })
      break

    case 'obtenerPorPagina':
      let numPagina = req.query.numPagina
      let numNoticiasPorPagina = Number(req.query.numNoticiasPorPagina)

      News.find({}, (err, news) => {
        if (err) {
          return res.status(500).send({ message: 'Ha ocurrido un error al consultar la base de datos. ' + err })
        }
        if (!news) {
          return res.status(404).send({ message: 'El elemento no existe en la base de datos.' })
        }

        res.status(200).send({ news: news })
      }).limit(numNoticiasPorPagina).skip((numPagina - 1) * numNoticiasPorPagina)
  }
}

function NewNews (req, res) {
  let news = new News()

  news.creator._id = req.body.creator._id
  news.creator.name = req.body.creator.name
  news.creator.date = req.body.creator.date
  news.guest_users_id = req.body.guest_users_id
  news.available = req.body.available
  news.tags = req.body.tags
  news.cover_url = req.body.cover_url
  news.images_url = req.body.images_url

  news.save((err, newsStored) => {
    if (err) {
      return res.status(500).send({ message: 'Ha ocurrido un error al intentar guardar los datos en la base de datos. ' + err })
    }

    res.status(200).send({ news: newsStored })
  })
}

function UpdateNews (req, res) {
  let id = req.params.id
  let body = req.body

  News.findByIdAndUpdate(id, body, (err, newsUpdated) => {
    if (err) {
      return res.status(500).send({ message: 'Ha ocurrido un error al intentar actualizar un elemento de la base de datos. ' + err })
    }

    res.status(200).send({ news: newsUpdated })
  })
}

function DeleteNews (req, res) {
  let id = req.params.id

  News.findById(id, (err, news) => {
    if (err) {
      return res.status(500).send({ message: 'Ha ocurrido un error al intentar borrar un elemento de la base de datos. ' + err })
    }

    news.remove(err => {
      if (err) {
        return res.status(500).send({ message: 'Ha ocurrido un error al intentar borrar un elemento de la base de datos. ' + err })
      }

      res.status(200).send({ message: 'El elemento ha sido borrado' })
    })
  })
}

module.exports = {
  GetNews,
  NewNews,
  UpdateNews,
  DeleteNews
}
