'use strict'

const File = require('../models/Files.js')

function GetFile (req, res) {
  let id = req.params.id

  File.findById(id, (err, file) => {
    if (err) {
      return res.status(500).send({ message: 'Ha ocurrido un error al consultar la base de datos. ' + err })
    }
    if (!file) {
      return res.status(404).send({ message: 'El elemento no existe en la base de datos.' })
    }

    res.status(200).send({ file: file })
  })
}

function GetFiles (req, res) {
  File.find({}, (err, files) => {
    if (err) {
      return res.status(500).send({ message: 'Ha ocurrido un error al consultar la base de datos. ' + err })
    }

    res.status(200).send({ files: files })
  })
}

function NewFile (req, res) {
  let file = new File()

  file.name = req.body.name
  file.type = req.body.filetype
  file.creator.id = req.body.creator.id
  file.creator.section = req.body.creator.section
  file.creator.name = req.body.creator.name
  file.creator.date = req.body.creator.date
  file.available = req.body.available
  file.url = req.body.url
  file.created = req.body.created

  file.save((err, fileStored) => {
    if (err) {
      return res.status(500).send({ message: 'Ha ocurrido un error al intentar guardar los datos en la base de datos. ' + err })
    }

    res.status(200).send({ file: fileStored })
  })
}

function UpdateFile (req, res) {
  let id = req.params.id
  let body = req.body

  File.findByIdAndUpdate(id, body, (err, fileUpdated) => {
    if (err) {
      return res.status(500).send({ message: 'Ha ocurrido un error al intentar actualizar un elemento de la base de datos. ' + err })
    }

    res.status(200).send({ file: fileUpdated })
  })
}

function DeleteFile (req, res) {
  let id = req.params.id

  File.findById(id, (err, file) => {
    if (err) {
      return res.status(500).send({ message: 'Ha ocurrido un error al intentar borrar un elemento de la base de datos. ' + err })
    }

    file.remove(err => {
      if (err) {
        return res.status(500).send({ message: 'Ha ocurrido un error al intentar borrar un elemento de la base de datos. ' + err })
      }

      res.status(200).send({ message: 'El elemento ha sido borrado' })
    })
  })
}

module.exports = {
  GetFile,
  GetFiles,
  NewFile,
  UpdateFile,
  DeleteFile
}