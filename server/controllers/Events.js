'use strict'

const Event = require('../models/Events.js')

function GetEvent (req, res) {
  let id = req.params.id

  Event.findById(id, (err, event) => {
    if (err) {
      return res.status(500).send({ message: 'Ha ocurrido un error al consultar la base de datos. ' + err })
    }
    if (!event) {
      return res.status(404).send({ message: 'El elemento no existe en la base de datos.' })
    }

    res.status(200).send({ event: event })
  })
}

function GetEvents (req, res) {
  Event.find({}, (err, events) => {
    if (err) {
      return res.status(500).send({ message: 'Ha ocurrido un error al consultar la base de datos. ' + err })
    }

    res.status(200).send({ events: events })
  })
}

function NewEvent (req, res) {
  let event = new Event()

  event.subject = req.body.subject
  event.description = req.body.description
  event.created = req.body.created
  event.guest_users_id = req.body.guest_users_id
  event.type = req.body.type
  event.date = req.body.date

  event.save((err, eventStored) => {
    if (err) {
      return res.status(500).send({ message: 'Ha ocurrido un error al intentar guardar los datos en la base de datos. ' + err })
    }

    res.status(200).send({ event: eventStored })
  })
}

function UpdateEvent (req, res) {
  let id = req.params.id
  let body = req.body

  Event.findByIdAndUpdate(id, body, (err, eventUpdated) => {
    if (err) {
      return res.status(500).send({ message: 'Ha ocurrido un error al intentar actualizar un elemento de la base de datos. ' + err })
    }

    res.status(200).send({ event: eventUpdated })
  })
}

function DeleteEvent (req, res) {
  let id = req.params.id

  Event.findById(id, (err, event) => {
    if (err) {
      return res.status(500).send({ message: 'Ha ocurrido un error al intentar borrar un elemento de la base de datos. ' + err })
    }

    event.remove(err => {
      if (err) {
        return res.status(500).send({ message: 'Ha ocurrido un error al intentar borrar un elemento de la base de datos. ' + err })
      }

      res.status(200).send({ message: 'El elemento ha sido borrado' })
    })
  })
}

module.exports = {
  GetEvent,
  GetEvents,
  NewEvent,
  UpdateEvent,
  DeleteEvent
}